package sample;

import javafx.application.Application;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.util.Arrays;
import java.util.Random;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
             BorderPane root = new BorderPane();

        //generator labyrinth in console
//element of labyrinth
        // |    -left element
        //   |  -right element
        // __   -down element
        //      -space
        String left = "|";
        String right = "  |";
        String space = "   ";
        String down = "___";
        String down_right = "__|";
//        String middle= "‾‾‾";
//        String middle = "---";
        //String labyrinth = "_";
        int SIZE_LABYRINTH = 20;
        String labyrinth[][] = new String[SIZE_LABYRINTH][SIZE_LABYRINTH];

        for (int i = 0; i < labyrinth.length; i++) {
            for (int j = 0; j < labyrinth.length; j++) {
                labyrinth[i][j] = space;
            }
        }

        Random random = new Random();
        int scores[] = new int[SIZE_LABYRINTH];
        int secondLine[] = new int[SIZE_LABYRINTH];
        for (int i = 0; i < scores.length; i++) {
            secondLine[i] = 0;
        }


//step 1
        for (int i = 0; i < SIZE_LABYRINTH; i++) {
            labyrinth[0][i] = down;
        }

//step 2
        for (int k = 1; k < SIZE_LABYRINTH; k++) {
            for (int i = 0; i < scores.length; i++) {
                if (secondLine[i] != 0) {
                    scores[i] = secondLine[i];
                } else {
                    scores[i] = i;
                }
            }
//step 3
            labyrinth[k][0] = left;
            for (int i = 1; i < SIZE_LABYRINTH - 1; i++) {
                if (scores[i] != scores[i + 1]) {
                    if (random.nextBoolean()) {
                        labyrinth[k][i] = right;
                    } else {
                        labyrinth[k][i] = space;
                        scores[i + 1] = scores[i];
                    }
                }
            }
            labyrinth[k][SIZE_LABYRINTH - 1] = right;

//step 4
            String[] arr1 = new String[SIZE_LABYRINTH];
            int number = scores[0];
            secondLine = Arrays.copyOf(scores, SIZE_LABYRINTH);
            for (int i = 0; i < scores.length - 1; i++) {
                if (number == scores[i + 1]) {
                    if (random.nextBoolean()) {
                        System.out.print(scores[i]+" ");
                        arr1[i] = down;
                    } else {
                        System.out.print(scores[i+1]+" ");
                        arr1[i + 1] = down;
                    }
                } else if (number != scores[i + 1]) {
                    number = scores[i + 1];
                }
            }
            System.out.println();

//            join
            for (int i = 0; i < SIZE_LABYRINTH; i++) {
                if (labyrinth[k][i] == right) {
                    if (arr1[i] == down) {
                        labyrinth[k][i] = down_right;
                    }
                }
                if (labyrinth[k][i] == space) {
                    if (arr1[i] == down) {
                        labyrinth[k][i] = down;
                    }
                }
            }

//step 5
            for (int i = 0; i < SIZE_LABYRINTH - 1; i++) {
                if (labyrinth[k][i] != down) {
                    secondLine[i] = 0;
                }
            }

//            if (k == SIZE_LABYRINTH - 1) {
//                for (int i = 0; i < SIZE_LABYRINTH; i++) {
//                    if (labyrinth[k][i] == space) {
//                        labyrinth[k][i] = down;
//                    }
//                    if (labyrinth[k][i] == right) {
//                        labyrinth[k][i] = down;
//                    }
//                }
//            }
        }

//result to terminal
        for (int i = 0; i < SIZE_LABYRINTH; i++) {
            for (int j = 0; j < SIZE_LABYRINTH; j++) {
                System.out.print(labyrinth[i][j]);
            }
            System.out.println();
        }

    }
//    result to window

//        TextArea map = new TextArea();
//        map.setText(labyrinth);
//        map.setEditable(false);
//        root.setCenter(map);
//        root.setStyle(
//                "-fx-font-size: 20;");
//
//        primaryStage.setScene(new Scene(root, 500, 500));
//        primaryStage.show();


    public static void main(String[] args) {
        launch(args);
    }
}
